package com.askin.shivansh.askinex;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;


public class CalendarFragment extends Fragment {

    //private OnFragmentInteractionListener mListener;

    public CalendarFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_calendar, container, false);

        CalendarView calendarView = (CalendarView) view.findViewById(R.id.calendar);
        final TextView textView = (TextView) view.findViewById(R.id.txtCalendar);
        textView.setMovementMethod(new ScrollingMovementMethod());

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
                String year = Integer.toString(i); String month = Integer.toString(i1); String day = Integer.toString(i2);
                if(i1 < 10)
                    month = "0" + i1;
                if(i2 < 10)
                    day = "0" + i2;

                ReadJsonFile readJsonFile = new ReadJsonFile();

                String json = readJsonFile.readFile(getActivity(), getString(R.string.json_string));
                String date = year + "-" + month + "-" + day + "T00:00:00.000Z";
                textView.setText("Selected Date: " + year + "-" + month + "-" + day);

                try {
                    JSONArray jsonArray = new JSONArray(json);
                    int length = jsonArray.length();
                    for(int j = 0; j < length; j += 1){
                        JSONObject jsonObject = jsonArray.getJSONObject(j);
                        Log.d("json ", getString(R.string.json_maintainanceDate));
                        String maintananceDate = jsonObject.getString(getString(R.string.json_maintainanceDate));
                        String createdDate = jsonObject.getString(getString(R.string.json_createdOn));
                        String modifiedDate = jsonObject.getString(getString(R.string.json_modifiedOn));
                        if(date.equals(maintananceDate) || date.equals(createdDate) || date.equals(modifiedDate)){
                            textView.append("\n\n" + getString(R.string.machineIdDisplay) + jsonObject.getString(getString(R.string.json_machineId)) +
                                    "\n" + getString(R.string.createdOnDisplay) + createdDate + "\n" + getString(R.string.maintenanceDateDisplay) + maintananceDate + "" +
                                    "\n" + getString(R.string.modifiedOnDisplay) + modifiedDate);
                        }
                    }
                }catch (org.json.JSONException e){

                }

            }
        });

        return view;
    }

    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    /*public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onCalendarFragmentInteraction(Uri uri);
    }*/
}
