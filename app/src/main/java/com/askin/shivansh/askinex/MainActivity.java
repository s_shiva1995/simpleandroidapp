package com.askin.shivansh.askinex;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.SharedPreferencesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences preferences = getSharedPreferences(getString(R.string.MY_PREFS), MODE_PRIVATE);
        String username = preferences.getString(getString(R.string.userName), null);

        //Toast.makeText(getApplicationContext(), username, Toast.LENGTH_LONG).show();

        if(username != null)
            //If user already logged in start Activity Header initially
            startActivity(new Intent(MainActivity.this, HeaderActivity.class));
        else
            //If user not logged in start Activity Login
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
        finish();
    }
}
