package com.askin.shivansh.askinex;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class HeaderActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //On click of Option in ActionBar
        switch (item.getItemId()){
            //Logging out the user
            case android.R.id.home:
                Toast.makeText(getApplicationContext(), getString(R.string.logout),Toast.LENGTH_LONG).show();
                SharedPreferences.Editor sharedPreferences = getSharedPreferences(getString(R.string.MY_PREFS), MODE_PRIVATE).edit();
                sharedPreferences.clear();
                sharedPreferences.apply();
                sharedPreferences.commit();
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_header);

        //Checking if preferences exist or not
        SharedPreferences preferences = getSharedPreferences(getString(R.string.MY_PREFS), MODE_PRIVATE);
        String userName = preferences.getString(getString(R.string.userName), null);

        Toast.makeText(getApplicationContext(), userName, Toast.LENGTH_LONG).show();

        //If preferences doesn't exist this activity is finished
        if(!(userName != null)){
            startActivity(new Intent(HeaderActivity.this, LoginActivity.class));
            finish();
        }

        //Setting up the page layout
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Attributes setup of action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Attributes setup of Fragments
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        //Attributes setup of Tab layout
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setupWithViewPager(viewPager);
    }

    /*
    * This function attaches the different fragments to this activity,
    * that is, ListFragment and Calendar Fragment
    */
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        //Adding ListFragment
        adapter.addFragment(new ListFragment(), getString(R.string.fragmentTabMachineList));
        //Adding CalendarFragment
        adapter.addFragment(new CalendarFragment(), getString(R.string.fragmentTabCalendar));
        viewPager.setAdapter(adapter);
    }

    /*
    * This class is used to setup the fragment and number of tabs in the activity
    */
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }
        //Return position of tab
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }
        //Return Number of Tabs
        @Override
        public int getCount() {
            return mFragmentList.size();
        }
        //Add fragments to the activity
        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
        //Return the title of the Tab
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
