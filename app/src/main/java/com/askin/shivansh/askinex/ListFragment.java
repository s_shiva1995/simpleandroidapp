package com.askin.shivansh.askinex;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ListViewAutoScrollHelper;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class ListFragment extends Fragment {

    //private OnFragmentInteractionListener mListener;

    public ListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        final ListView listView = (ListView) view.findViewById(R.id.list_item);

        //Reading JSONFile
        ReadJsonFile readJsonFile = new ReadJsonFile();
        final String json = readJsonFile.readFile(getActivity(), getString(R.string.json_string));

        List<String> list = new ArrayList<String>();

        try {

            JSONArray jsonArray = new JSONArray(json);
            int length = jsonArray.length();

            for(int i = 0; i < length; i += 1){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String machineName = jsonObject.getString(getString(R.string.json_machineId));
                list.add(machineName);
            }
        }catch (org.json.JSONException e){
            Log.d("Exception ", e.toString());
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_expandable_list_item_1, list);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String item = (String)listView.getItemAtPosition(i);

                //Info will be displayed in AlertDialog box
                String info = "";

                try{
                    //Getting data from JSONArray
                    JSONArray jsonArray= new JSONArray(json);
                    int length = jsonArray.length();
                    for(int j = 0; j < length; j += 1){
                        //Accessing JSONObject
                        JSONObject jsonObject = jsonArray.getJSONObject(j);
                        String machineName = jsonObject.getString(getString(R.string.json_machineId));
                        //Checking for the selected item and JSONArray list
                        if(machineName.equals(item)){
                            Log.d("Machine Name ", machineName + " " + item);
                            //Updating info variable
                            info += getString(R.string.machineNameDisplay) + machineName + "\n" +
                                    getString(R.string.machineIdDisplay) + jsonObject.getString(getString(R.string.json_id)) + "\n" +
                                    getString(R.string.machineGroupDisplay) + jsonObject.getString(getString(R.string.json_machineGroupId)) + "\n" +
                                    getString(R.string.amcDateDisplay) + jsonObject.getString(getString(R.string.json_amcDate)).substring(0, 10) + "\n" +
                                    getString(R.string.maintenanceDateDisplay) + jsonObject.getString(getString(R.string.json_maintainanceDate)).substring(0, 10) + "\n" +
                                    getString(R.string.statusDisplay) + jsonObject.getString(getString(R.string.json_status)) + "\n" +
                                    getString(R.string.createdByDisplay) + jsonObject.getString(getString(R.string.json_createdBy)) + "\n" +
                                    getString(R.string.createdOnDisplay) + jsonObject.getString(getString(R.string.json_createdOn)).substring(0, 10) + "\n" +
                                    getString(R.string.modifiedByDisplay) + jsonObject.getString(getString(R.string.json_modifiedBy)) + "\n" +
                                    getString(R.string.modifiedOnDisplay) + jsonObject.getString(getString(R.string.json_modifiedOn)).substring(0, 10);

                            Log.d("Information ", info);
                            break;
                        }
                    }
                }catch (org.json.JSONException e){

                }

                //Building AlertDialog Box
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                        getActivity());

                // Setting Dialog Title
                alertDialog.setTitle(getString(R.string.alertDialogTitle));

                // Setting Dialog Message
                alertDialog.setMessage(info);

                // Setting OK Button
                alertDialog.setPositiveButton(getString(R.string.alertDialogPositive), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // User pressed YES button. Write Logic Here
                        Toast.makeText(getActivity(), "You clicked on YES",
                                Toast.LENGTH_SHORT).show();
                    }
                });

                alertDialog.setNegativeButton(getString(R.string.alertDialogNegative), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to invoke NO event
                        Toast.makeText(getActivity(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                        dialog.cancel();
                    }
                });

                alertDialog.create();
                // Showing Alert Message
                alertDialog.show();
            }
        });

        return view;
    }

    /*public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onListFragmentInteraction(Uri uri);
    }*/
}
