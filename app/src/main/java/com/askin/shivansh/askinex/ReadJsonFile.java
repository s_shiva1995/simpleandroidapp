package com.askin.shivansh.askinex;

import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by root on 20/7/17.
 */

public class ReadJsonFile {

    public String readFile(Context context, String FileName){

        //FileName = context.getResources().openRawResource();
        try{
            InputStream fileInputStream = context.getResources().openRawResource(R.raw.string);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder json = new StringBuilder();
            String line = "";
            while((line = bufferedReader.readLine()) != null){
                json.append(line);
            }
            return json.toString();
        }catch (java.io.FileNotFoundException e) {
            return null;
        }catch (java.io.IOException e){
            return null;
        }
    }

    public boolean isFilePresent(Context context, String FileName){
        String path = context.getResources() + "/" + FileName;
        File file = new File(path);
        return file.exists();
    }

}
